from conans import ConanFile, CMake, tools

class YueConan(ConanFile):
    name = "yue"
    version = "0.8.8"
    settings = "os", "compiler", "build_type", "arch"
    description = "A library for creating native cross-platform GUI apps https://libyue.com"
    url = "https://bitbucket.org/toge/conan-yue"
    license = "LGPL v2.1 with an exception"

    def source(self):
        url_base = "https://github.com/yue/yue/releases/download/v%s/libyue_v%s_%s.zip"

        url = ""
        if (self.settings.os == "Linux"):
            url = url_base % (self.version, self.version, "linux")
        elif (self.settings.os == "Windows"):
            url = url_base % (self.version, self.version, "win")
        elif (self.settings.os == "Macos"):
            url = url_base % (self.version, self.version, "mac")

        if (url == ""):
            raise Exception("Binary does not exist for these settings")

        self.output.info("Downloading %s" % url)
        tools.get(url)
        tools.replace_in_file("CMakeLists.txt", "if(CMAKE_SOURCE_DIR STREQUAL CMAKE_CURRENT_SOURCE_DIR)", "if(0)")

    def build(self):
        cmake = CMake(self)
        cmake.verbose = True
        cmake.configure(source_folder=".")
        cmake.build()

    def package(self):
        self.copy("*.h", src="include", dst="include")
        self.copy("*.a", dst="lib", keep_path=False)

    def package_info(self):
        self.cpp_info.libs = ["glib", "Yue", "glib"]
        if self.settings.compiler == "gcc":
            self.cpp_info.cflags = ["-D_GLIBCXX_USE_CXX11_ABI=1", "-fdata-sections", "-ffunction-sections", "-Wno-deprecated-declarations"]

